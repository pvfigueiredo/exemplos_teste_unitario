program Calculadora;

uses
  Vcl.Forms,
  Main in 'views\Main.pas' {frmMain},
  CalculadoraController in 'controllers\CalculadoraController.pas',
  ControllerInterfaces in 'controllers\ControllerInterfaces.pas',
  CalculadoraModel in 'models\CalculadoraModel.pas',
  ModelsInterface in 'models\ModelsInterface.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
