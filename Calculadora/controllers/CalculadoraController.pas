unit CalculadoraController;

interface

uses
  ControllerInterfaces, System.Generics.Collections, ModelsInterface,
  CalculadoraModel, SysUtils;

type
  TCalculadoraController = class(TInterfacedObject, ICalculadoraController)
  private
    FValueA: double;
    FValueB: double;
    FOperador: TOperador;
  public
    constructor Create(Entrada: string);
    destructor Destroy; override;
    class function New(Entrada: string): ICalculadoraController;
    function Calcula: double;
    function ExtraiDadosEntrada(Entrada: string): ICalculadoraController;
    function GetOperador(Entrada: string): TOperador; overload;
    function GetOperador(Operador: Char): TOperador; overload;
  end;

implementation

uses
  System.Classes;

function TCalculadoraController.Calcula: double;
begin
  case FOperador of
    Multiplicacao: Result := TCalculadoraModel.New.Multiplica(FValueA, FValueB);
    Divisao: Result := TCalculadoraModel.New.Divide(FValueA, FValueB);
    Soma: Result := TCalculadoraModel.New.Soma(FValueA, FValueB);
    Subtracao: Result := TCalculadoraModel.New.Subtrai(FValueA, FValueB);
  end;
end;

constructor TCalculadoraController.Create(Entrada: string);
begin
  ExtraiDadosEntrada(Entrada)
end;

destructor TCalculadoraController.Destroy;
begin
  inherited;
end;

class function TCalculadoraController.New(Entrada: string): ICalculadoraController;
begin
  Result := Self.Create(Entrada);
end;

function TCalculadoraController.ExtraiDadosEntrada(
  Entrada: string): ICalculadoraController;
var
  Parametros: TArray<String>;
begin
  Parametros := Entrada.Split(['*', '/', '+', '-', '%']);
  FValueA := StrToFloat(Parametros[0]);
  FOperador := GetOperador(Entrada);
  FValueB := StrToFloat(Parametros[1]);
end;

function TCalculadoraController.GetOperador(Entrada: string): TOperador;
var
  Operador: Char;
begin
  for Operador in Entrada do
    if Operador in ['*', '/', '+', '-'] then
    begin
      Result := GetOperador(Operador);
      break;
    end;
end;

function TCalculadoraController.GetOperador(Operador: Char): TOperador;
begin
  case Operador of
    '*' : Result := Multiplicacao;
    '/' : Result := Divisao;
    '+' : Result := Soma;
    '-' : Result := Subtracao;
  end;
end;
end.
