unit ControllerInterfaces;

interface

uses
  System.Generics.Collections;

type
  TOperador = (Multiplicacao, Divisao, Soma, Subtracao);
  ICalculadoraController = interface
    ['{CC97CFD0-FBBD-4C57-BE4B-129D5031D08E}']
    function Calcula: double;
    function ExtraiDadosEntrada(Entrada: string): ICalculadoraController;
    function GetOperador(Entrada: string): TOperador; overload;
    function GetOperador(Operador: Char): TOperador; overload;
  end;

implementation

end.
