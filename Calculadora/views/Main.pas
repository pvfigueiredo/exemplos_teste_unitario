﻿unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Generics.Collections,
  System.Classes;

type
  TfrmMain = class(TForm)
    btn7: TButton;
    btn8: TButton;
    btn9: TButton;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn0: TButton;
    BtnSubtrai: TButton;
    btnSoma: TButton;
    btnDivide: TButton;
    btnMultiplica: TButton;
    btnPonto: TButton;
    btnInverteSinal: TButton;
    edtTelaOperacoes: TEdit;
    btnPorcentagem: TButton;
    btnClean: TButton;
    btnBackSpace: TButton;
    btnResultado: TButton;

    procedure btn0Click(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
    procedure btn7Click(Sender: TObject);
    procedure btn8Click(Sender: TObject);
    procedure btn9Click(Sender: TObject);

    procedure btnDivideClick(Sender: TObject);
    procedure btnMultiplicaClick(Sender: TObject);
    procedure btnSomaClick(Sender: TObject);
    procedure BtnSubtraiClick(Sender: TObject);
    procedure btnPontoClick(Sender: TObject);
    procedure edtTelaOperacoesKeyPress(Sender: TObject; var Key: Char);
    procedure btnBackSpaceClick(Sender: TObject);
    procedure btnCleanClick(Sender: TObject);
    procedure btnResultadoClick(Sender: TObject);

  private
    { Private declarations }
    FOperador: Char;
    FNumeros: TList<double>;
    procedure ImprimeTelaOperacoes(Sender: TObject);
    function EhOperador(const Key: Char): boolean;
    function ValidaEntrada(const Key: Char): boolean;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  CalculadoraModel, CalculadoraController;

{$R *.dfm}

procedure TfrmMain.btn0Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn1Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn2Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn3Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn4Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn5Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn6Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn7Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn8Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btn9Click(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btnBackSpaceClick(Sender: TObject);
begin
  edtTelaOperacoes.Text := Copy(edtTelaOperacoes.Text, 0, Length(edtTelaOperacoes.Text) - 1)
end;

procedure TfrmMain.btnCleanClick(Sender: TObject);
begin
  edtTelaOperacoes.Clear;
end;

procedure TfrmMain.btnDivideClick(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btnMultiplicaClick(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btnPontoClick(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.btnResultadoClick(Sender: TObject);
begin
  try
    edtTelaOperacoes.Text := TCalculadoraController
                              .New(edtTelaOperacoes.Text)
                              .Calcula
                              .ToString;
  except
    on E: Exception do
      edtTelaOperacoes.Text := E.Message;
  end;
end;

procedure TfrmMain.btnSomaClick(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.BtnSubtraiClick(Sender: TObject);
begin
  ImprimeTelaOperacoes(Sender);
end;

procedure TfrmMain.edtTelaOperacoesKeyPress(Sender: TObject; var Key: Char);
begin
  if not(ValidaEntrada(Key)) then
    Key := #0;
end;

function TfrmMain.EhOperador(const Key: Char): boolean;
begin
  Result := Key in ['/', '*', '-', '+', '%'];
end;

procedure TfrmMain.ImprimeTelaOperacoes(Sender: TObject);
begin
  edtTelaOperacoes.Text := edtTelaOperacoes.Text + TButton(Sender).Caption;
end;

function TfrmMain.ValidaEntrada(const Key: Char): boolean;
begin
  Result := (Key in [#8, '0'..'9', ',']) or (EhOperador(Key));
end;

end.
