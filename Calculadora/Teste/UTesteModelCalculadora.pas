unit UTesteModelCalculadora;

interface
uses
  DUnitX.TestFramework, CalculadoraModel;

type

  [TestFixture]
  TTesteModelCalculadora = class(TObject)
  public
    [Setup]
    procedure Setup;
    [TearDown]
    procedure TearDown;
    // Sample Methods
    // Simple single Test
    // Test with TestCase Attribute to supply parameters.
    [Test]
    [TestCase('TestA','15,5,20')]
    [TestCase('TestB','3,4,7')]
    procedure TesteSoma(const AValue1, AValue2, _Result : double);
    [Test]
    [TestCase('TestA','15,5,7')]
    procedure TesteSubtrai(const AValue1, AValue2, _Result : double);
    [Test]
    [TestCase('TestA','15,3')]
    procedure TesteExceptionDivisao(const AValue1, AValue2: double);

  end;

implementation

procedure TTesteModelCalculadora.Setup;
begin
end;

procedure TTesteModelCalculadora.TearDown;
begin
end;

procedure TTesteModelCalculadora.TesteSoma(const AValue1, AValue2, _Result : double);
begin
  Assert.AreEqual(_Result, TCalculadoraModel.New.Soma(AValue1, AValue2));
end;

procedure TTesteModelCalculadora.TesteSubtrai(const AValue1, AValue2, _Result : double);
begin
  Assert.AreNotEqual(_Result, TCalculadoraModel.New.Subtrai(AValue1, AValue2));
end;

procedure TTesteModelCalculadora.TesteExceptionDivisao(const AValue1, AValue2: double);
begin
  Assert.WillRaiseAny(procedure
                      begin
                        TCalculadoraModel.New.Divide(AValue1, AValue2);
                      end,
                      'Erro! Divis�o por 0!');
end;


initialization
  TDUnitX.RegisterTestFixture(TTesteModelCalculadora);
end.
