object frmMain: TfrmMain
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Calculadora'
  ClientHeight = 257
  ClientWidth = 185
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btn7: TButton
    Left = 8
    Top = 87
    Width = 41
    Height = 41
    Caption = '7'
    TabOrder = 1
    OnClick = btn7Click
  end
  object btn8: TButton
    Left = 48
    Top = 87
    Width = 41
    Height = 41
    Caption = '8'
    TabOrder = 2
    OnClick = btn8Click
  end
  object btn9: TButton
    Left = 88
    Top = 87
    Width = 41
    Height = 41
    Caption = '9'
    TabOrder = 3
    OnClick = btn9Click
  end
  object btn4: TButton
    Left = 8
    Top = 127
    Width = 41
    Height = 41
    Caption = '4'
    TabOrder = 4
    OnClick = btn4Click
  end
  object btn5: TButton
    Left = 48
    Top = 127
    Width = 41
    Height = 41
    Caption = '5'
    TabOrder = 5
    OnClick = btn5Click
  end
  object btn6: TButton
    Left = 88
    Top = 127
    Width = 41
    Height = 41
    Caption = '6'
    TabOrder = 6
    OnClick = btn6Click
  end
  object btn1: TButton
    Left = 8
    Top = 167
    Width = 41
    Height = 41
    Caption = '1'
    TabOrder = 7
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 48
    Top = 167
    Width = 41
    Height = 41
    Caption = '2'
    TabOrder = 8
    OnClick = btn2Click
  end
  object btn3: TButton
    Left = 88
    Top = 167
    Width = 41
    Height = 41
    Caption = '3'
    TabOrder = 9
    OnClick = btn3Click
  end
  object btn0: TButton
    Left = 48
    Top = 207
    Width = 41
    Height = 41
    Caption = '0'
    TabOrder = 10
    OnClick = btn0Click
  end
  object BtnSubtrai: TButton
    Left = 135
    Top = 167
    Width = 41
    Height = 41
    Caption = '-'
    TabOrder = 11
    OnClick = BtnSubtraiClick
  end
  object btnSoma: TButton
    Left = 135
    Top = 127
    Width = 41
    Height = 41
    Caption = '+'
    TabOrder = 12
    OnClick = btnSomaClick
  end
  object btnDivide: TButton
    Left = 135
    Top = 47
    Width = 41
    Height = 41
    Caption = '/'
    TabOrder = 13
    OnClick = btnDivideClick
  end
  object btnMultiplica: TButton
    Left = 135
    Top = 87
    Width = 41
    Height = 41
    Caption = '*'
    TabOrder = 14
    OnClick = btnMultiplicaClick
  end
  object btnPonto: TButton
    Left = 88
    Top = 207
    Width = 41
    Height = 41
    Caption = ','
    TabOrder = 15
    OnClick = btnPontoClick
  end
  object btnInverteSinal: TButton
    Left = 8
    Top = 207
    Width = 41
    Height = 41
    Caption = '+/-'
    TabOrder = 16
  end
  object edtTelaOperacoes: TEdit
    Left = 8
    Top = 8
    Width = 169
    Height = 21
    Alignment = taRightJustify
    TabOrder = 0
    OnKeyPress = edtTelaOperacoesKeyPress
  end
  object btnPorcentagem: TButton
    Left = 8
    Top = 47
    Width = 41
    Height = 41
    Caption = '%'
    TabOrder = 17
  end
  object btnClean: TButton
    Left = 48
    Top = 47
    Width = 41
    Height = 41
    Caption = 'C'
    TabOrder = 18
    OnClick = btnCleanClick
  end
  object btnBackSpace: TButton
    Left = 88
    Top = 47
    Width = 41
    Height = 41
    Caption = #8592
    TabOrder = 19
    OnClick = btnBackSpaceClick
  end
  object btnResultado: TButton
    Left = 135
    Top = 207
    Width = 41
    Height = 41
    Caption = '='
    TabOrder = 20
    OnClick = btnResultadoClick
  end
end
