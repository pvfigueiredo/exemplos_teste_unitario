unit ModelsInterface;

interface

type
  ICalculadoraModel = interface
    ['{D8D0F038-D94A-4666-97E3-6B626EEAA4F5}']
    function Soma(const ValueA, ValueB: double): double;
    function Subtrai(const ValueA, ValueB: double): double;
    function Multiplica(const ValueA, ValueB: double): double;
    function Divide(const ValueA, ValueB: double): double;
    function Porcentagem(const ValueA, ValueB: double): double;
  end;
implementation

end.
